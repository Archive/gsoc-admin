#!/usr/bin/env python3

import datetime
import smtplib
from email.mime.text import MIMEText


def send_mail(text, subject, sender, recipient, cc, bcc):
    mimobj = MIMEText(text)
    mimobj['From'] = sender
    mimobj['To'] = recipient
    mimobj['Subject'] = subject
    mimobj['CC'] = cc
    mimobj['BCC'] = bcc

    localhost = smtplib.SMTP('localhost')
    localhost.send_message(mimobj)
    localhost.quit()


class Contact:
    def __init__(self, mail, name=None):
        self.mail = mail
        self._name = name

    @property
    def name(self):
        return self._name or "Unknown Name"

    def pretty_email_name(self):
        return "{name} <{mail}>".format(name=self.name, mail=self.mail)


class EmailTemplate:
    def __init__(self,
                 text,
                 subject,
                 sender,
                 recipients,
                 data_dict,
                 cc="",
                 bcc="",
                 date=None):
        """
        Creates a new mail template.

        :param text:       The text with data fields marked like {key}.
        :param subject:    Subject of the email (string).
        :param sender:     The mail adress of the sender (string).
        :param recipients: A list of Contact objects.
        :param data_dict:  A dictionary to fill the text.
        :param cc:         The mail adress(es) to CC (string).
        :param bcc:        The mail adress(es) to BCC (string).
        :param date:       An Arrow object indicating when this template is to
                           be sent.
        """
        now = datetime.datetime.now()
        data_dict.update({
            'year': now.year,
            'month': now.month,
            'day': now.day,
            'hour': now.hour,
            'minute': now.minute})

        self.text = text
        self.subject = subject
        self.sender = sender
        self.recipients = recipients
        self.data_dict = data_dict
        self.cc = cc
        self.bcc = bcc
        self.date = date

    def get_specific(self):
        """
        Applies the data to the template.
        """
        return self.text.format(**self.data_dict)

    def send(self):
        for recipient in self.recipients:
            self.data_dict['recipient_name'] = recipient.name
            send_mail(self.get_specific(),
                      self.subject,
                      self.sender,
                      recipient.pretty_email_name(),
                      self.cc,
                      self.bcc)
