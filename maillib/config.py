#!/usr/bin/env python3

import configparser
import datetime
import csv
import re
from ics import Calendar
from urllib import request
from datetime import timedelta
from importlib import __import__

from maillib.email import Contact, EmailTemplate


def apply_substitutions(uri):
    now = datetime.datetime.now()
    substitutions = {
        'year': now.year,
        'month': now.month,
        'day': now.day,
        'hour': now.hour,
        'minute': now.minute}
    return uri.format(**substitutions)


def get_main_data_from_section(section):
    """
    This one will search for 'url' or 'path' settings, eventually supply data
    to them and get the data wanted.
    """
    assert ('url' in section) != ('path' in section)
    if 'url' in section:
        source = apply_substitutions(section['url'])
        return request.urlopen(source).read().decode()
    else:
        source = apply_substitutions(section['path'])
        with open(source) as file:
            return file.read()


def get_mail_data_source(path, event_config):
    get_data = __import__(path, fromlist=('get_data',)).get_data
    return get_data(event_config)


class EventConfig:
    def __init__(self):
        self.ics_sources = {}
        self.mail_templates = []
        self.dates = {}
        self.csvs = {}

    def append_from_section(self, section):
        section_type = section['type'].lower()
        if section_type in ['date']:
            return self.append_date_from_section(section)

        if section_type in ['mail', 'mail_template']:
            return self.append_mail_from_section(section)

        if section_type in ['ics', 'ics_source']:
            return self.append_ics_from_section(section)

        if section_type in ['csv', 'csv_source']:
            return self.append_csv_from_section(section)

    def append_date_from_section(self, section):
        events = self.ics_sources[section['ics_source']].events
        regex = section['description_regex']
        if not regex.endswith('$'):
            regex += '$'

        compiled = re.compile(regex)
        for event in events:
            if (
                    (event.description is not None and
                     compiled.match(event.description) is not None) or
                    (event.name is not None and
                     compiled.match(event.name))):
                self.dates[section.name] = event

        assert section.name in self.dates, ("There are no events matching the "
                                            "regular xpression '{}'."
                                            .format(regex))

    def append_mail_from_section(self, section):
        text = get_main_data_from_section(section)
        sender = section['sender']
        subject = section['subject']
        cc = section.get('cc', '')
        bcc = section.get('bcc', '')
        recipients_mail_column = section['recipients_mail_column']
        recipients_name_column = section.get('recipients_name_column', None)
        recipients = []
        for row in self.csvs[section['recipients']]:
            mail = row[recipients_mail_column]
            name = row.get(recipients_name_column, None)
            recipients.append(Contact(mail, name))

        days_before = int(section.get('days_before', '0'))
        related_to = self.dates[section['related_to']].begin
        related_to -= timedelta(days=days_before)

        data_source = section.get('data_source')
        if data_source is not None:
            data_dict = get_mail_data_source(data_source, self)
        else:
            data_dict = {}

        self.mail_templates.append(
            EmailTemplate(text, subject, sender, recipients,
                          data_dict, cc, bcc, related_to))

    def append_ics_from_section(self, section):
        ics_data = get_main_data_from_section(section)
        self.ics_sources[section.name] = Calendar(ics_data)

    def append_csv_from_section(self, section):
        csv_data = get_main_data_from_section(section)
        reader = csv.DictReader(csv_data.split('\n'),
                                delimiter=section.get('delimiter', ','))
        self.csvs[section.name] = list(reader)


def parse_event_config(filename):
    config = configparser.ConfigParser()
    config.read(filename)
    result = EventConfig()
    for section in config.sections():
        result.append_from_section(config[section])

    return result
