from maillib.config import EventConfig


def get_data(event_config):
    assert isinstance(event_config, EventConfig)
    mentors = []
    students = []
    form_data = event_config.csvs['GSoC_Students']
    for row in form_data:
        mentors.append(row['mentor'])
        student = "Name: {}\nMail: {}\nMentor: {}\nProject: {}\n" \
                  .format(row['name'], row['mail'], row['mentor'],
                          row['project'])
        students.append(student)

    return {'mentors': ', '.join(mentors),
            'projects': '\n'.join(students)}
