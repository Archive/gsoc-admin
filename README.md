# Purpose

This repository holds scripts for automating GSoC administration.

See https://wiki.gnome.org/Outreach/SummerOfCode/2016/AdminIdeas

# Tests

To execute tests, just run `python3 -m pytest`.

# TODO

OpenShift deployment should be easy to do. Probably a python 3.3 cartridge will
suffice.
