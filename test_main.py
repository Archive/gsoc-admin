#!/usr/bin/env python3

from maillib.config import parse_event_config

if __name__ == '__main__':
    c = parse_event_config("data/config.cfg")
    print("TEMPLATE")
    print(c.mail_templates[0].text)
    print("SPECIFIC")
    print(c.mail_templates[0].get_specific())
    print("TO BE SENT ON:")
    print(c.mail_templates[0].date)
    print("SUBJECT:")
    print(c.mail_templates[0].subject)
