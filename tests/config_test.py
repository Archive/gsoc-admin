import datetime
import unittest
from maillib.config import apply_substitutions, parse_event_config


class ModuleTest(unittest.TestCase):
    def test_apply_substitutions(self):
        now = datetime.datetime.now()
        self.assertEqual(apply_substitutions("some/{year}"),
                         "some/"+str(now.year))
        self.assertEqual(apply_substitutions("some/year"), "some/year")

    def test_parse_config(self):
        config = parse_event_config("data/config.cfg")
        self.assertEqual(config.mail_templates[0].text, """Dear interns, mentors and GNOME contributors,

we hereby welcome all our interns and are happy to have our awesome
mentors with us: {mentors} - welcome everyone!

Cheers,

Your GSoC Administrators

---

{projects}
""")
        self.assertEqual(config.mail_templates[0].get_specific(), """Dear interns, mentors and GNOME contributors,

we hereby welcome all our interns and are happy to have our awesome
mentors with us: Zeeshan Ali Khattak, Christophe Fergeau - welcome everyone!

Cheers,

Your GSoC Administrators

---

Name: Lasse Schuirmann
Mail: lasse.schuirmann@gmail.com
Mentor: Zeeshan Ali Khattak
Project: Boxes

Name: Alexandre Franke
Mail: afranke@gnome.org
Mentor: Christophe Fergeau
Project: GSoC Administration

""")
        self.assertEqual(config.mail_templates[0].subject, 'Welcome Interns!')
